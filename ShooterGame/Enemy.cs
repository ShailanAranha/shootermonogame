﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShooterGame
{
    class Enemy
    {
        /// <summary>
        /// Handles Enemy Animation
        /// </summary>
        public Animation m_enemyAnimation = null;

        /// <summary>
        /// The position of the enemy ship relative to the top left corner of the screen
        /// </summary>
        public Vector2 m_vec2Position = Vector2.Zero;

        /// <summary>
        /// The state of the Enemy Ship
        /// </summary>
        public bool m_isActive = false;

        /// <summary>
        /// The hit points of the enemy, if this goes to zero the enemy dies
        /// </summary>
        public int m_iHealth = -1;

        /// <summary>
        /// The amount of damage the enemy inflicts on the player ship
        /// </summary>
        public int m_iDamage = -1;

        /// <summary>
        /// The amount of score the enemy will give to the player
        /// </summary>
        public int m_iValue = -1;

        /// <summary>
        /// Get the width of the enemy ship
        /// </summary>
        public int Width
        {
            get
            {
                return m_enemyAnimation.m_iFrameWidth;
            }
        }

        /// <summary>
        /// Get the height of the enemy ship 
        /// </summary>
        public int Height
        {
            get
            {
                return m_enemyAnimation.m_iFrameHeight;
            }
        }

        /// <summary>
        /// Get the height of the enemy ship 
        /// </summary>
        private float m_fltEnemyMoveSpeed = 0.0f;

        /// <summary>
        /// Initiaizes all the objects
        /// </summary>
        public void Initialize(Animation animation, Vector2 position)
        {
            m_enemyAnimation = animation;
            m_vec2Position = position;
            m_isActive = true;
            m_iHealth = 10;
            m_iDamage = 10;
            m_fltEnemyMoveSpeed = 6f;
            m_iValue = 100;
        }

        /// <summary>
        /// Enemy Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        public void Update(GameTime gameTime)
        {
            m_vec2Position.X -= m_fltEnemyMoveSpeed;

            m_enemyAnimation.m_vec2Position = m_vec2Position;

            m_enemyAnimation.Update(gameTime);
            
            if (m_vec2Position.X < -Width || m_iHealth <= 0)
            {
                m_isActive = false;
            }
        }

        /// <summary>
        /// Renders components on screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public void Draw(SpriteBatch a_spriteBatch)
        {
            // Draw the animation
            m_enemyAnimation.Draw(a_spriteBatch);
        }
    }
}
