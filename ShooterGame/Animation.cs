﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ShooterGame
{
    class Animation
    {
        /// <summary>
        /// Image representing the collection of images used for animation
        /// </summary>
        private Texture2D m_spriteStrip = null;

        /// <summary>
        /// The scale used to siaply the spirte strip
        /// </summary>
        private float m_fltScale = 0.0f;

        /// <summary>
        /// Time since the last frame update
        /// </summary>
        private int m_iElapsedTime = -1;

        /// <summary>
        /// Time we display a frame until the next frame
        /// </summary>
        private int m_iFrameTime = -1;

        /// <summary>
        /// Number of frames the animation contains
        /// </summary>
        private int m_iFrameCount = -1;

        /// <summary>
        /// The index of the current frame we are displaying
        /// </summary>
        private int m_iCurrentFrame = -1;

        /// <summary>
        /// The color of the frame we will be displaying
        /// </summary>
        private Color m_color = Color.Black;

        /// <summary>
        /// The area of image strip we want to display
        /// </summary>
        private Rectangle m_rectSource = new Rectangle();
        private Rectangle m_rectDestination = new Rectangle();

        /// <summary>
        /// Width and heigh of the given frame
        /// </summary>
        public int m_iFrameWidth = -1;
        public int m_iFrameHeight = -1;

        /// <summary>
        /// State of animation
        /// </summary>
        public bool m_isActive = false;

        /// <summary>
        /// Determines if the animation will keep playing or deactivate after one run
        /// </summary>
        public bool m_isLooping = false;

        /// <summary>
        /// CurrentPosition
        /// </summary>
        public Vector2 m_vec2Position;

        public void Initialize(Texture2D a_texture, Vector2 a_vec2Pos, int a_iFrameWidth, int a_iFrameHeight, int a_iFrameCount, 
            int a_iFrameTime, Color a_color, float a_fltScale, bool a_isLooping)
        {
            // keeping local copy of the values passed    
            this.m_color = a_color;
            this.m_iFrameWidth = a_iFrameWidth;
            this.m_iFrameHeight = a_iFrameHeight;
            this.m_iFrameCount = a_iFrameCount;
            this.m_iFrameTime = a_iFrameTime;
            this.m_fltScale = a_fltScale;

            m_isLooping = a_isLooping;
            m_vec2Position = a_vec2Pos;
            m_spriteStrip = a_texture;

            // Set the time to 0
            m_iElapsedTime = 0; 
            m_iCurrentFrame = 0;

            m_isActive = true;
        }

        public void Update(GameTime a_gametime)
        {
            if (!m_isActive)
            {
                return;
            }

            m_iElapsedTime += (int)a_gametime.ElapsedGameTime.TotalMilliseconds;

            if (m_iElapsedTime > m_iFrameTime)
            {
                m_iCurrentFrame++;

                if (m_iCurrentFrame == m_iFrameCount)
                {
                    m_iCurrentFrame = 0;

                    if (!m_isLooping)
                    {
                        m_isActive = false;
                    }
                }

                m_iElapsedTime = 0;
            }

            m_rectSource = new Rectangle(m_iCurrentFrame * m_iFrameWidth, 0, m_iFrameWidth, m_iFrameHeight);

            m_rectDestination = new Rectangle((int)m_vec2Position.X - (int)(m_iFrameWidth * m_fltScale) / 2,
                (int)m_vec2Position.Y - (int)(m_iFrameHeight * m_fltScale) / 2, (int)(m_iFrameWidth * m_fltScale), (int)(m_iFrameHeight * m_fltScale));
        }

        
        public void Draw(SpriteBatch a_spriteBacth)
        {
            if (m_isActive)
            {
                a_spriteBacth.Draw(m_spriteStrip, m_rectDestination, m_rectSource, m_color);
            }
        }


    }
}
