﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShooterGame
{
    class Laser
    {
        /// <summary>
        /// Laser animation
        /// </summary>
        public Animation m_laserAnimation = null;

        /// <summary>
        /// the speed the laser travels 
        /// </summary>
        private float m_fltLaserMoveSpeed = 30.0f;

        /// <summary>
        /// CurrentPosition of the laser
        /// </summary>
        public Vector2 m_vec2Position = Vector2.Zero;

        /// <summary>
        /// The damage the laser deals.
        /// </summary>
        public int m_iDamage = 10;

        /// <summary>
        ///  set the laser to active
        /// </summary>
        public bool m_isActive = false;

        /// <summary>
        /// Laser beams range.
        /// </summary>
        private int m_iRange = -1;

        /// <summary>
        /// the width of the laser image.
        /// </summary>
        public int Width
        {
            get
            {
                return m_laserAnimation.m_iFrameWidth;
            }
        }

        /// <summary>
        /// the width of the laser image.
        /// </summary>
        public int Height
        {
            get { return m_laserAnimation.m_iFrameHeight; }
        }

        /// <summary>
        /// Initiaizes all the objects
        /// </summary>
        public void Initialize(Animation animation, Vector2 position)
        {
            m_laserAnimation = animation;
            m_vec2Position = position;
            m_isActive = true;
        }

        /// <summary>
        /// Laser Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        public void Update(GameTime gameTime)
        {
            m_vec2Position.X += m_fltLaserMoveSpeed;
            m_laserAnimation.m_vec2Position = m_vec2Position;
            m_laserAnimation.Update(gameTime);
        }

        /// <summary>
        /// Renders components on screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            m_laserAnimation.Draw(spriteBatch);
        }
    }
}
