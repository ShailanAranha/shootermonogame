﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace ShooterGame
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        /// <summary>
        /// Represents player
        /// </summary>
        private Player m_player = null;

        /// <summary>
        /// Keyboard states used to determine key presses
        /// </summary>
        private KeyboardState m_kbCurrentKeyboardState;
        private KeyboardState m_kbPreviousKeyboardState;

        /// <summary>
        /// Gamepad states used to determine button presses
        /// </summary>
        private GamePadState m_gpCurrentGamePadState;
        private GamePadState m_gpPreviousGamePadState;

        /// <summary>
        /// Mouse States used to track mouse button press
        /// </summary>
        private MouseState m_mosCurrentMouseState;
        private MouseState m_mosPreviousMouseState;

        /// <summary>
        /// A movement speed for the layer
        /// </summary>
        private float m_fltPlayerMoveSpeed = 0.0f;

        /// <summary>
        /// Image used to display the static background
        /// </summary>
        private Texture2D m_texMainBackground = null;

        /// <summary>
        /// Parallax layers
        /// </summary>
        private ParallaxingBackground m_bgLayer1;
        private ParallaxingBackground m_bgLayer2;

        /// <summary>
        /// Enemy
        /// </summary>
        private Texture2D m_enemyTexture = null;
        private List<Enemy> m_lstEnemies = null;

        /// <summary>
        /// The rate at which the enemies appear
        /// </summary>
        private TimeSpan m_enemySpawnTime;
        private TimeSpan m_PreviousenemySpawnTime;

        /// <summary>
        /// A random number generator
        /// </summary>
        private Random m_random;

        /// <summary>
        /// Texture to hold the laser.
        /// </summary>
        private Texture2D m_texLaser = null;

        /// <summary>
        /// Govern how fast our laser can fire.
        /// </summary>
        private TimeSpan m_laserSpawnTime;
        private TimeSpan m_previousLaserSpawnTime;

        /// <summary>
        /// Laser beams
        /// </summary>
        private List<Laser> m_lstLaserBeams = null;

        /// <summary>
        /// Collection of explosions
        /// </summary>
        private List<Explosion> m_lstExplosions = null;

        /// <summary>
        /// Explosion texture
        /// </summary>
        private Texture2D m_texExplosion = null;

        /// <summary>
        /// Our Laser Sound and Instance
        /// </summary>
        private SoundEffect laserSound;
        private SoundEffectInstance laserSoundInstance;

        /// <summary>
        /// Our Explosion Sound. 
        /// </summary>
        private SoundEffect explosionSound;
        private SoundEffectInstance explosionSoundInstance;

        /// <summary>
        /// Background music object
        /// </summary>
        private Song gameMusic;

        /// <summary>
        /// The font used to display UI elements
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// Number that holds the player score
        /// </summary>
        private int score;

        /// <summary>
        /// Game over? (Player health reached 0)
        /// </summary>
        private bool m_isGameOver = false;

        /// <summary>
        /// Is Start Screen Started?
        /// </summary>
        private bool m_isStartScreenStarted = false;

        /// <summary>
        /// Default constructor
        /// </summary>
        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Initiaizes all the objects
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            m_player = new Player();
            m_bgLayer1 = new ParallaxingBackground();
            m_bgLayer2 = new ParallaxingBackground();
            m_fltPlayerMoveSpeed = 8.0f;

            m_lstEnemies = new List<Enemy>();

            m_PreviousenemySpawnTime = TimeSpan.Zero;
            m_enemySpawnTime = TimeSpan.FromSeconds(1.0f);

            m_random = new Random();

            m_lstLaserBeams = new List<Laser>();
            const float SECONDS_IN_MINUTE = 60.0f;
            const float RATE_OF_FIRE = 200.0f;
            m_laserSpawnTime = TimeSpan.FromSeconds(SECONDS_IN_MINUTE / RATE_OF_FIRE);

            m_lstExplosions = new List<Explosion>();

            score = 0;

            base.Initialize();

        }

        /// <summary>
        /// Loads game resources
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //Vector2 m_vec2PlayerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
            //    GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);

            //m_player.Initialize(Content.Load<Texture2D>("Graphics\\player"), m_vec2PlayerPosition);

            Animation l_playerAnimation = new Animation();
            Texture2D l_playerTexture = Content.Load<Texture2D>("Graphics\\shipAnimation");
            l_playerAnimation.Initialize(l_playerTexture, Vector2.Zero, 115, 69, 8, 30, Color.White, 1.0f, true);

            Vector2 l_playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X, GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);
            m_player.Initialize(l_playerAnimation, l_playerPosition);
            m_player.StartPosition = l_playerPosition;

            m_bgLayer1.Initialize(Content, "Graphics/bgLayer1", GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, -1);
            m_bgLayer2.Initialize(Content, "Graphics/bgLayer2", GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, -2);
            m_texMainBackground = Content.Load<Texture2D>("Graphics/mainbackground");
            m_enemyTexture = Content.Load<Texture2D>("Graphics/mineAnimation");
            m_texLaser = Content.Load<Texture2D>("Graphics\\laser");
            m_texExplosion = Content.Load<Texture2D>("Graphics\\explosion");

            // Load the laserSound Effect and create the effect Instance 
            laserSound = Content.Load<SoundEffect>("Sound\\laserFire");
            laserSoundInstance = laserSound.CreateInstance();

            explosionSound = Content.Load<SoundEffect>("Sound\\explosion");
            explosionSoundInstance = explosionSound.CreateInstance();

            gameMusic = Content.Load<Song>("Sound\\gameMusic");
            MediaPlayer.Play(gameMusic);

            font = Content.Load<SpriteFont>("Graphics\\gameFont");
        }

        /// <summary>
        /// Unloads game resources
        /// </summary>
        protected override void UnloadContent()
        {
            MediaPlayer.Stop();
            laserSoundInstance.Dispose();
            explosionSoundInstance.Dispose();
            base.UnloadContent();
        }

        /// <summary>
        /// Main Game loop
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();


            // Save the previous state of the keyboard, game pad, and mouse so we can determine key / button presses
            m_gpPreviousGamePadState = m_gpCurrentGamePadState;
            m_kbPreviousKeyboardState = m_kbCurrentKeyboardState;
            m_mosPreviousMouseState = m_mosCurrentMouseState;

            // Read the current state and store it
            m_kbCurrentKeyboardState = Keyboard.GetState();
            m_gpCurrentGamePadState = GamePad.GetState(PlayerIndex.One);
            m_mosCurrentMouseState = Mouse.GetState();

            // Update the player
            UpdatePlayer(gameTime);

            // Update the enemies
            UpdateEnemies(gameTime);
            UpdateCollision();

            m_bgLayer1.Update(gameTime);
            m_bgLayer2.Update(gameTime);

            UpdateLaserBeams(gameTime);

            UpdateExplosions(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// Renders components on screen
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            _spriteBatch.Begin();
            _spriteBatch.Draw(m_texMainBackground, Vector2.Zero, Color.White);
            m_bgLayer1.Draw(_spriteBatch);
            m_bgLayer2.Draw(_spriteBatch);
            m_player.Draw(_spriteBatch);

            for (int i = 0; i < m_lstEnemies.Count; i++)
            {
                m_lstEnemies[i].Draw(_spriteBatch);
            }

            foreach (Laser l in m_lstLaserBeams)
            {
                l.Draw(_spriteBatch);
            }

            // draw explosions
            foreach (var e in m_lstExplosions)
            {
                e.Draw(_spriteBatch);
            }

            if (!m_isStartScreenStarted)
            {
                _spriteBatch.DrawString(font, "SHOOTER ", new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X + GraphicsDevice.Viewport.Width / 2 - 50, GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.Height / 2 - 100), Color.White);
                _spriteBatch.DrawString(font, "PRESS SPACE/X/RIGHT CLICK TO SHOOT!!", new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X + GraphicsDevice.Viewport.Width / 2 - 220, GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.Height / 2), Color.White);

            }
            else if (m_isGameOver)
            {
                // Draw the score
                _spriteBatch.DrawString(font, "GAME OVER!! \nYOUR SCORE: " + score + "\nPRESS ENTER/A/MIDDLE CLICK TO RESTART!!", new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X / 2, GraphicsDevice.Viewport.TitleSafeArea.Y / 2), Color.White);
            }
            else
            {
                // Draw the score
                _spriteBatch.DrawString(font, "score: " + score, new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X, GraphicsDevice.Viewport.TitleSafeArea.Y), Color.White);

                // Draw the player health
                _spriteBatch.DrawString(font, "health: " + m_player.Health, new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X, GraphicsDevice.Viewport.TitleSafeArea.Y + 30), Color.White);
            }

            _spriteBatch.End();

            base.Draw(gameTime);
        }


        /// <summary>
        /// Update for player components (Helper function)
        /// </summary>
        /// <param name="a_gameTime"></param>
        private void UpdatePlayer(GameTime a_gameTime)
        {
            if (!m_isStartScreenStarted)
            {
                if (m_kbCurrentKeyboardState.IsKeyDown(Keys.Enter) ||
                    m_kbCurrentKeyboardState.IsKeyDown(Keys.Space) ||
                    m_gpCurrentGamePadState.Buttons.A == ButtonState.Pressed ||
                    m_gpCurrentGamePadState.Buttons.X == ButtonState.Pressed ||
                    m_mosCurrentMouseState.MiddleButton == ButtonState.Pressed ||
                    m_mosCurrentMouseState.LeftButton == ButtonState.Pressed ||
                    m_mosCurrentMouseState.RightButton == ButtonState.Pressed)
                {
                    
                    m_isStartScreenStarted = true;
                }
                return;
            }

            if (m_isGameOver)
            {
                if (m_kbCurrentKeyboardState.IsKeyDown(Keys.Enter) || m_gpCurrentGamePadState.Buttons.A == ButtonState.Pressed || m_mosCurrentMouseState.MiddleButton == ButtonState.Pressed)
                {
                    m_player.CurrentPosition = m_player.StartPosition;
                    m_player.Health = 100;
                    score = 0;
                    m_isGameOver = false;
                }
                return;
            }

            m_player.Update(a_gameTime);

            //Get Mouse State then Capture the Button type and Respond Button Press
            Vector2 l_mousePosition = new Vector2(m_mosCurrentMouseState.X, m_mosCurrentMouseState.Y);

            if (m_mosCurrentMouseState.LeftButton == ButtonState.Pressed)
            {
                //if (!m_mosCurrentMouseState.Equals(m_mosPreviousMouseState))
                //{
                //    Vector2 l_posDelta = l_mousePosition - m_player.CurrentPosition;
                //    l_posDelta.Normalize();
                //    l_posDelta = l_posDelta * m_fltPlayerMoveSpeed;
                //    m_player.CurrentPosition = m_player.CurrentPosition + l_posDelta;
                //}


                Vector2 l_posDelta = l_mousePosition - m_player.CurrentPosition;

                if ((l_posDelta.X < 1.5f && l_posDelta.X > -1.5f) || (l_posDelta.Y < 1.5f && l_posDelta.Y > -1.5f))
                {

                }
                else
                {
                    l_posDelta.Normalize();
                    l_posDelta = l_posDelta * m_fltPlayerMoveSpeed;
                    m_player.CurrentPosition = m_player.CurrentPosition + l_posDelta;
                }
            }

            // get thumstick controls
            m_player.CurrentPosition.X += m_gpCurrentGamePadState.ThumbSticks.Left.X * m_fltPlayerMoveSpeed;
            m_player.CurrentPosition.Y -= m_gpCurrentGamePadState.ThumbSticks.Left.Y * m_fltPlayerMoveSpeed;

            // Use the keyboard / Dpad

            if (m_kbCurrentKeyboardState.IsKeyDown(Keys.Left) || m_gpCurrentGamePadState.DPad.Left == ButtonState.Pressed)
            {
                m_player.CurrentPosition.X -= m_fltPlayerMoveSpeed;
            }

            if (m_kbCurrentKeyboardState.IsKeyDown(Keys.Right) || m_gpCurrentGamePadState.DPad.Right == ButtonState.Pressed)
            {
                m_player.CurrentPosition.X += m_fltPlayerMoveSpeed;
            }

            if (m_kbCurrentKeyboardState.IsKeyDown(Keys.Up) || m_gpCurrentGamePadState.DPad.Up == ButtonState.Pressed)
            {
                m_player.CurrentPosition.Y -= m_fltPlayerMoveSpeed;
            }

            if (m_kbCurrentKeyboardState.IsKeyDown(Keys.Down) || m_gpCurrentGamePadState.DPad.Down == ButtonState.Pressed)
            {
                m_player.CurrentPosition.Y += m_fltPlayerMoveSpeed;
            }

            // RO MAKE SURE THE PLAYER DOESN;T GO OUT OF BOUNDS

            m_player.CurrentPosition.X = MathHelper.Clamp(m_player.CurrentPosition.X, m_player.Width / 2, GraphicsDevice.Viewport.Width - m_player.Width / 2);
            m_player.CurrentPosition.Y = MathHelper.Clamp(m_player.CurrentPosition.Y, m_player.Height / 2, GraphicsDevice.Viewport.Height - m_player.Height / 2);

            if (m_kbCurrentKeyboardState.IsKeyDown(Keys.Space) || m_gpCurrentGamePadState.Buttons.X == ButtonState.Pressed || m_mosCurrentMouseState.RightButton == ButtonState.Pressed)
            {
                FireLaser(a_gameTime);
            }

            // reset score if player health goes to zero
            if (m_player.Health <= 0)
            {
                m_isGameOver = true;
                ResetData();
                //m_player.Health = 100;
                //score = 0;
            }
        }

        /// <summary>
        /// Add anemy in the game
        /// </summary>
        private void AddEnemy()
        {
            Animation l_enemyAnimation = new Animation();
            l_enemyAnimation.Initialize(m_enemyTexture, Vector2.Zero, 47, 61, 8, 30, Color.White, 1f, true);
            Vector2 l_position = new Vector2(GraphicsDevice.Viewport.Width + m_enemyTexture.Width / 2,
            m_random.Next(100, GraphicsDevice.Viewport.Height - 100));
            Enemy enemy = new Enemy();
            enemy.Initialize(l_enemyAnimation, l_position);
            m_lstEnemies.Add(enemy);
        }

        /// <summary>
        /// Update for enemy components
        /// </summary>
        /// <param name="gameTime"></param>
        private void UpdateEnemies(GameTime gameTime)
        {
            if (!m_isStartScreenStarted)
            {
                return;
            }

            if (m_isGameOver)
            {
                return;
            }

            // Spawn a new enemy enemy every 1.5 seconds
            if (gameTime.TotalGameTime - m_PreviousenemySpawnTime > m_enemySpawnTime)
            {
                m_PreviousenemySpawnTime = gameTime.TotalGameTime;
                // Add an Enemy
                AddEnemy();
            }
            // Update the Enemies
            for (int i = m_lstEnemies.Count - 1; i >= 0; i--)
            {
                m_lstEnemies[i].Update(gameTime);
                if (!m_lstEnemies[i].m_isActive)
                {
                    // score += m_lstEnemies[i].m_iValue;
                    m_lstEnemies.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Update for collision components
        /// </summary>
        private void UpdateCollision()
        {
            Rectangle l_playerRectangle;
            Rectangle l_enemyRectangle;

            l_playerRectangle = new Rectangle((int)m_player.CurrentPosition.X, (int)m_player.CurrentPosition.Y, m_player.Width, m_player.Height);

            for (int i = 0; i < m_lstEnemies.Count; i++)
            {
                l_enemyRectangle = new Rectangle((int)m_lstEnemies[i].m_vec2Position.X, (int)m_lstEnemies[i].m_vec2Position.Y, m_lstEnemies[i].Width, m_lstEnemies[i].Height);

                if (l_playerRectangle.Intersects(l_enemyRectangle))
                {
                    m_player.Health -= m_lstEnemies[i].m_iDamage;
                    m_lstEnemies[i].m_iHealth = 0;

                    if (m_player.Health <= 0)
                    {
                        m_player.Active = false;
                    }
                }

                if (l_playerRectangle.Intersects(l_enemyRectangle))
                {
                    m_lstEnemies[i].m_iHealth = 0;
                    AddExplosion(m_lstEnemies[i].m_vec2Position);
                    m_player.Health -= m_lstEnemies[i].m_iDamage;
                    if (m_player.Health <= 0)
                    {
                        m_player.Active = false;
                    }
                }

                Rectangle l_laserRectangle;

                for (var l = 0; l < m_lstLaserBeams.Count; l++)
                {
                    l_laserRectangle = new Rectangle((int)m_lstLaserBeams[l].m_vec2Position.X, (int)m_lstLaserBeams[l].m_vec2Position.Y, m_lstLaserBeams[l].Width, m_lstLaserBeams[l].Height);
                    //if (l_laserRectangle.Intersects(l_enemyRectangle))
                    //{
                    //    m_lstEnemies[i].m_iHealth = 0;
                    //    m_lstLaserBeams[l].m_isActive = false;
                    //}

                    // test the bounds of the laser and enemy 
                    if (l_laserRectangle.Intersects(l_enemyRectangle))
                    {
                        AddExplosion(m_lstEnemies[i].m_vec2Position);
                        m_lstEnemies[i].m_iHealth = 0;
                        m_lstLaserBeams[l].m_isActive = false;

                        score += m_lstEnemies[i].m_iValue;
                    }


                }

            }

        }

        /// <summary>
        /// Fires laser at enemy
        /// </summary>
        /// <param name="gameTime"></param>
        protected void FireLaser(GameTime gameTime)
        {
            // govern the rate of fire for our lasers
            if (gameTime.TotalGameTime - m_previousLaserSpawnTime > m_laserSpawnTime)
            {
                m_previousLaserSpawnTime = gameTime.TotalGameTime;
                AddLaser();
                laserSoundInstance.Play();
            }
        }

        /// <summary>
        /// Initiate laser
        /// </summary>
        protected void AddLaser()
        {
            Animation laserAnimation = new Animation();
            laserAnimation.Initialize(m_texLaser, m_player.CurrentPosition, 46, 16, 1, 30, Color.White, 1f, true);
            Laser laser = new Laser();
            var laserPostion = m_player.CurrentPosition;
            laserPostion.X += 30;
            laser.Initialize(laserAnimation, laserPostion);
            m_lstLaserBeams.Add(laser);

            /* todo: add code to create a laser. */
            // laserSoundInstance.Play();
        }

        /// <summary>
        /// Update for laser
        /// </summary>
        /// <param name="a_gametime"></param>
        private void UpdateLaserBeams(GameTime a_gametime)
        {
            for (int i = m_lstLaserBeams.Count - 1; i >= 0; i--)
            {
                m_lstLaserBeams[i].Update(a_gametime);

                if (!m_lstLaserBeams[i].m_isActive)
                {
                    m_lstLaserBeams.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Initiates explosion
        /// </summary>
        /// <param name="enemyPosition"></param>
        protected void AddExplosion(Vector2 enemyPosition)
        {
            Animation explosionAnimation = new Animation();
            explosionAnimation.Initialize(m_texExplosion, enemyPosition, 134, 134, 12, 30, Color.White, 1.0f, true);
            Explosion explosion = new Explosion();
            explosion.Initialize(explosionAnimation, enemyPosition);
            m_lstExplosions.Add(explosion);
            // explosionSoundInstance.Play();
            explosionSound.Play();
        }

        /// <summary>
        /// Update for explosins
        /// </summary>
        /// <param name="gameTime"></param>
        private void UpdateExplosions(GameTime gameTime)
        {
            for (var e = m_lstExplosions.Count - 1; e >= 0; e--)
            {
                m_lstExplosions[e].Update(gameTime);

                if (!m_lstExplosions[e].m_isActive)
                    m_lstExplosions.Remove(m_lstExplosions[e]);
            }
        }

        /// <summary>
        /// Reset data
        /// </summary>
        private void ResetData()
        {
            for (int i = 0; i < m_lstEnemies.Count; i++)
            {
                m_lstEnemies[i].m_iHealth = 0;
            }

            m_lstEnemies.Clear();
        }
    }
}
