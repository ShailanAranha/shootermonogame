﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ShooterGame
{
    class ParallaxingBackground
    {
        /// <summary>
        /// Background image
        /// </summary>
        private Texture2D m_texture = null;

        /// <summary>
        /// Array of positions for the bg
        /// </summary>
        private Vector2[] m_arrPositions = null;

        /// <summary>
        /// Speed of the background
        /// </summary>
        private int m_iSpeed = -1;

        /// <summary>
        /// Bg height and width
        /// </summary>
        int m_iBgHeight = -1;
        int m_iBgWidth = -1;

        /// <summary>
        /// Initiaizes all the objects
        /// </summary>
        public void Initialize(ContentManager a_content, string a_strTexturePath, int a_iScreenWidth, int a_iScreenHeight, int a_iSpeed)
        {
            m_iBgHeight = a_iScreenHeight;
            m_iBgWidth = a_iScreenWidth;

            m_texture = a_content.Load<Texture2D>(a_strTexturePath);

            this.m_iSpeed = a_iSpeed;

            int l_iNumOfTiles = (int)(Math.Ceiling(a_iScreenWidth / (float)m_texture.Width) + 1);
            m_arrPositions = new Vector2[l_iNumOfTiles];

            for (int i = 0; i < m_arrPositions.Length; i++)
            {
                m_arrPositions[i] = new Vector2(i* m_texture.Width, 0);
            }

        }

        /// <summary>
        /// Backgroung Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        public void Update(GameTime a_gameTime)
        {

            for (int i = 0; i < m_arrPositions.Length; i++)
            {
                m_arrPositions[i].X += m_iSpeed;

                if (m_iSpeed <= 0)
                {

                    if (m_arrPositions[i].X <= -m_texture.Width)
                    {
                        WrapTextureToLeft(i);
                    }
                }
                else
                {
                    if (m_arrPositions[i].X >= m_texture.Width * (m_arrPositions.Length - 1))
                    {
                        WrapTextureToRight(i);
                    }
                }
            }
        }

        /// <summary>
        /// Renders components on screen
        /// </summary>
        public void Draw(SpriteBatch a_spriteBatch)
        {
            for (int i = 0; i < m_arrPositions.Length; i++)
            {
                Rectangle rectBg = new Rectangle((int)m_arrPositions[i].X, (int)m_arrPositions[i].Y, m_iBgWidth,m_iBgHeight);
                a_spriteBatch.Draw(m_texture, rectBg, Color.White);
            }
        }

        /// <summary>
        /// Handles tecture position on left side
        /// </summary>
        /// <param name="a_iIndex"></param>
        private void WrapTextureToLeft(int a_iIndex)
        {
            int a_prevTexture = a_iIndex - 1;

            if (a_prevTexture < 0)
            {
                a_prevTexture = m_arrPositions.Length - 1;
            }

            m_arrPositions[a_iIndex].X = m_arrPositions[a_prevTexture].X + m_texture.Width - 2;
        }

        /// <summary>
        /// Handles tecture position on right side
        /// </summary>
        /// <param name="a_iIndex"></param>
        private void WrapTextureToRight(int a_iIndex)
        {
            int a_iNextTexture = a_iIndex + 1;
            if (a_iNextTexture == m_arrPositions.Length)
                a_iNextTexture = 0;

            m_arrPositions[a_iIndex].X = m_arrPositions[a_iNextTexture].X - m_texture.Width + 2;
        }
    }
}
