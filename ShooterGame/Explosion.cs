﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShooterGame
{
    class Explosion
    {
        /// <summary>
        /// Explosin Animation
        /// </summary>
        private Animation m_animExplosion = null;

        /// <summary>
        /// Explosion position
        /// </summary>
        private Vector2 m_vec2Position = Vector2.Zero;

        /// <summary>
        /// Is explosion active?
        /// </summary>
        public bool m_isActive = false;

        /// <summary>
        /// Total time on screen
        /// </summary>
        private int m_iTimeToLive = -1;

        /// <summary>
        /// Explosion width
        /// </summary>
        public int Width
        {
            get
            {
                return m_animExplosion.m_iFrameWidth;
            }
        }

        /// <summary>
        /// Explosion height
        /// </summary>
        public int Height
        {
            get
            {
                return m_animExplosion.m_iFrameHeight;
            }
        }

        /// <summary>
        /// Initiaizes all the objects
        /// </summary>
        public void Initialize(Animation a_animation, Vector2 a_position)
        {
            m_animExplosion = a_animation;
            m_vec2Position = a_position;
            m_isActive = true;
            m_iTimeToLive = 30;
        }

        /// <summary>
        /// Laser Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        public void Update(GameTime a_gametime)
        {
            m_animExplosion.Update(a_gametime);
            m_iTimeToLive -= 1;
            if (m_iTimeToLive <= 0)
            {
                this.m_isActive = false;
            }
        }

        /// <summary>
        /// Renders components on screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public void Draw(SpriteBatch a_spriteBatch)
        {
            m_animExplosion.Draw(a_spriteBatch);
        }
    }
}
