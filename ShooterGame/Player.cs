﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShooterGame;

namespace ShooterGame
{
    class Player
    {
        /// <summary>
        /// Animation representing the player
        /// </summary>
       // public Texture2D PlayerTexture { get; set; } = null;

        /// <summary>
        /// Player animation object
        /// </summary>
        public Animation PlayerAnimation;

        /// <summary>
        /// CurrentPosition of the player relative to the upper left side of the screen
        /// </summary>
        public Vector2 CurrentPosition = Vector2.Zero;

        /// <summary>
        /// Player Start positin
        /// </summary>
        public Vector2 StartPosition = Vector2.Zero;

        /// <summary>
        /// State of the player
        /// </summary>
        public bool Active { get; set; } = false;

        /// <summary>
        /// Amount of hit points that player has
        /// </summary>
        public int Health { get; set; } = -1;

        /// <summary>
        /// Get the width of the player ship
        /// </summary>
        public int Width
        {
            get
            {
                return PlayerAnimation.m_iFrameWidth;
            }
        }

        /// <summary>
        /// Get the height of the player ship
        /// </summary>
        public int Height
        {
            get
            {
                return PlayerAnimation.m_iFrameHeight;
            }
        }


        //public void Initialize(Texture2D a_texture, Vector2 a_vec2Position)
        //{
        //    PlayerTexture = a_texture;
        //    CurrentPosition = a_vec2Position;
        //    Active = true;
        //    Health = 100;
        //}

        /// <summary>
        /// Initiaizes all the objects
        /// </summary>
        public void Initialize(Animation a_anim, Vector2 a_vec2Position)
        {
            PlayerAnimation = a_anim;
            CurrentPosition = a_vec2Position;
            Active = true;
            Health = 100;
        }

        /// <summary>
        /// Player Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        public void Update(GameTime a_gameTime)
        {
            PlayerAnimation.m_vec2Position = CurrentPosition;
            PlayerAnimation.Update(a_gameTime);

        }

        /// <summary>
        /// Renders components on screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public void Draw(SpriteBatch a_spriteBatch)
        {
            //Vector2 a_vec2DrawPosition = new Vector2(CurrentPosition.X- Width/2, CurrentPosition.Y- Health/2);
            //a_spriteBatch.Draw(PlayerTexture, a_vec2DrawPosition, null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None,0.0f);

            PlayerAnimation.Draw(a_spriteBatch);
        }
    }
}
